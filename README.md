# Crud Interns Api

An adonis application that exposes CRUD functionalities for interns

## link to Endpoints

[CrudEndpoints](https://www.getpostman.com/collections/e70caf1896e60ce156e7)
## user requirements
 - Create interns resource endpoints that expose crud functionalities for interns.
  - The intern objects being mocked should contain the following properties.
    - Name of the intern
    - Email of the intern
    - An array of the tech stack the user shall build an app with


## Setup

Use the adonis command to install the blueprint

```bash
adonis new yardstick
```

or manually clone the repo and then run `npm install`.


### Migrations

Run the following command to run startup migrations.

```js
adonis migration:run
```
