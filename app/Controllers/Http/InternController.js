'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with interns
 */
let interns = [{
  id: 0,
  name: "Dorcas Cherono",
  email: "dorcas@sendyit.com",
  stacks: ["Vue","Adonis"]
},
{
  id: 1,
  name: "Stacey Chebet ",
  email: "stacey@sendyit.com",
  stacks: ["Flutter","Adonis"]
},
{
  id: 2,
  name: "Gill Erick",
  email: "gill@sendyit.com",
  stacks: ["Flutter","Quarkus"]
},
{
  id: 3,
  name: "Maxwell Kimaiyo",
  email: "maxwell@sendyit.com",
  stacks: ["Vue","Goz"]
},
]
const { validate } = use('Validator')
class InternController {

  constructor() {
  }
  /**
   * Show a list of all interns.
   * GET interns
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {

    response.send(interns);
  }

  /**
   * Render a form to be used for creating a new intern.
   * GET interns/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new intern.
   * POST interns
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    const rules = {
      email: 'required|email',
      name: 'required|string',
      stacks:'required'

    }

    const internsObject = await validate(request.all(), rules)
    if (internsObject.fails()) {
      response.send({
        message: "Input all the fields correctly!"
      })
    }
    else {
      interns.push(internsObject)
      return response.send(interns)
    }


  }

  /**
   * Display a single intern.
   * GET interns/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params: { id }, request, response, view }) {
    const one_intern = interns.find(one_intern => one_intern.id == id);
    return response.send(one_intern)

  }

  /**
   * Render a form to update an existing intern.
   * GET interns/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update intern details.
   * PUT or PATCH interns/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const rules = {
      email: 'email',
      name: 'string',
      stacks:'string'
    }
    let internsData = request.only(['name', 'email', 'stacks'])
    let id = Number(params.id)

    let interndata = {
      id: id, 
      name: internsData.name,
      email: internsData.email,
      stacks: internsData.stacks,
    }
    interns = interns.map((item) => {
      return item.id === id ? interndata : item
    })
    console.log(internsData)
    return interndata

  }

  /**
   * Delete a intern with id.
   * DELETE interns/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params:{id}, request, response }) {
    const one_intern = interns.find(one_intern => one_intern.id == id);
    let index = interns.indexOf(one_intern)
    interns.splice(index, 1)
    response.status(200).send({
      message:`The object with id ${index} has been deleted` 
    })

  }
}

module.exports = InternController
